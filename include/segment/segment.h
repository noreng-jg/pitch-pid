#ifndef SEGMENT_H
#define SEGMENT_H
#include <string>
#include <vector>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/core.hpp"

class Segment {
  public:
    Segment(std::string filename);
    int getImageHeight();
    int getImageWidth();
    void filterSegments();
    std::vector<std::array<float, 4>> getLineSegments();
    void showImage();
  private:
    cv::Mat image;
    cv::Ptr<cv::LineSegmentDetector> det;
    int imageheight;
    int imagewidth;
    std::vector<cv::Vec4f> linesegments;
};

#endif
