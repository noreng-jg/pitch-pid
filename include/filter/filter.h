#ifndef FILTER_H
#define FILTER_H

#include <vector>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

typedef struct {
  float x1;
  float y1;
  float x2;
  float y2;
  float v1x;
  float v1y;
  float v2x;
  float v2y;
} Points;

bool inAPreviousRange(Points *p, cv::Vec4f &point);;
std::vector<cv::Vec4f> filteredSegments(std::vector<cv::Vec4f> lines);
void insertionControl(cv::Vec4f point, std::vector<cv::Vec4f> &vec);
bool hasVerticalLines(int y1, int y2);
bool repeatedNote(Points *p);
bool belowPercentageLimit(float v1, float v2, float perc);
float _module(float val);
float evaluateCloserFrequency(float liminf, float limsup, float number);
bool isBetween(float liminf, float limsup, float number);

#endif
