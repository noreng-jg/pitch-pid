#include <iostream>
#include "spectrogram.h"
#include "filter.h"
#include <algorithm>

Spectrogram::Spectrogram(int h, int w, float timed) {
  height = h;
  width = w;
  timeduration = timed;

  std::vector<OctaveRange> octaveRanges;

  for (int i = 0; i < 7; i++) {
    float fratio = (pow(2, (12-i)) - pow(2, (12-1-i)))/(h/7);

    octaveRanges.push_back(OctaveRange{
        highestpixel: i*(h/7),
        lowestpixel: (i+1)*(h/7),
        fmax: pow(2, 12-i),
        fmin: pow(2, 12-1-i),
        num: i+1,
        freqratio: fratio,
        limpowermin: (12 -i),
        limpowermax: (12 -1-i),
    });
  }

  this->octaveRanges = octaveRanges;
}

void Spectrogram::printOctaveRangesInfo() {
  /*
     This is an auxiliar function for debugging
   */
  int count =0;
  for (auto i = this->octaveRanges.begin(); i != this->octaveRanges.end(); i++) {
    count++;
    std::cout << "the highest pixel for octave "<< count << " : "<< (*i).highestpixel << std::endl;
    std::cout << "the lowest pixel for octave "<< count << " : "<< (*i).lowestpixel << std::endl;
    std::cout << "the max freq for octave "<< count << " : "<< (*i).fmax << std::endl;
    std::cout << "the min freq for octave "<< count << " : "<< (*i).fmin << std::endl;
    std::cout << "the freq ration for octave "<< count << " : "<< (*i).freqratio << std::endl;
    std::cout << "the limpowermin for octave "<< count << " : "<< (*i).limpowermin << std::endl;
    std::cout << "the limpowermax for octave "<< count << " : "<< (*i).limpowermax << std::endl;
    std::cout << "\n\n " <<std::endl;;
  }
}

Pitch Spectrogram::pitchDetector(float frequency) {
    for(auto i=mapFrequencies.begin(); i!=mapFrequencies.end(); i++){
      auto next = std::next(i, 1);
        if (isBetween(i->first, next-> first, frequency)) {
          float estimation = evaluateCloserFrequency(i->first, next->first, frequency);
          std::cout << this->mapFrequencies[estimation].label << std::endl;
          if (mapFrequencies[estimation].label == "")  {
            // when there is no response from estmation choose below
            return this->mapFrequencies[i->first];
          }
          return mapFrequencies[estimation];
        }
      }

    Pitch t = {4, "F#6/Gb6"}; //must be in the middle spectrum
    return t;
}

int Spectrogram::getHeight() {
  return this->height;
}

float Spectrogram::estimatedFreq(float pi) {
  /*
    // BUG = There is an issue when the freq is 220.44 which doenst fit
    in any octave range
 */

  for (auto i = this->octaveRanges.begin(); i != this->octaveRanges.end(); i++) {
    std::cout << pi << " "<< (*i).highestpixel << " " << (*i).lowestpixel << std::endl;
    if (pi > (*i).highestpixel && pi <= (*i).lowestpixel)  {
      return (*i).fmax + ((*i).highestpixel - pi)*((*i).freqratio);
    }
  }
  return -1;
}

NoteTimeProperties Spectrogram::evaluateNoteTimeProperties(float begin, float end) {
  NoteTimeProperties ntp = {
    begin: begin*(this->timeduration/this->width),
    end: end*(this->timeduration/this->width),
    duration: _module((end-begin)*(this->timeduration/this->width)),
  };

  return ntp;
}

void Spectrogram::noteDetector(std::vector<std::array<float, 4>> pointCoordinates) {
  /*
     Based on points coordinates analysis detect the time
     and pitch content for the note
   */
    std::vector<Note> det_arr;

    for (std::array<float, 4> pc: pointCoordinates) {
      std::cout << "In note detector" << std::endl;
      std::cout << "x1: " <<  pc[0] << " " << "y1: " <<  pc[1] << std::endl;
      std::cout << "x2: " <<  pc[2] << " " << "y2: " <<  pc[3] << std::endl;

      float medianPitch;
      medianPitch = (pc[1] + pc[3])/2;

      std::cout << "the median pitch is " << medianPitch << std::endl;

      float freq = estimatedFreq(medianPitch);
      Pitch pitch = pitchDetector(freq);

      std::cout << "Freq: " << freq << " map freq is " << this->mapFrequencies[freq].label << std::endl;

      NoteTimeProperties nt;
      nt = this->evaluateNoteTimeProperties(pc[0], pc[2]);

      Note n = {
        pitch: pitch,
        ntp: nt,
      };

      det_arr.push_back(n);

      std::cout << "Time properties below: " << std::endl;
      std::cout << "begin: " << nt.begin << " end: " << nt.end << " duration: "<< nt.duration << std::endl;
    }

    this->detectedNotesArr = det_arr;
}

void Spectrogram::sortNotes() {
  std::vector<Note> notes = this->detectedNotesArr;

  std::sort(notes.begin(),notes.end(), [](const Note &a, const Note &b){
      return a.ntp.begin < b.ntp.begin;
  });

  this->detectedNotesArr = notes;
}

void Spectrogram::saveDetectedNotes(std::string filename) {
  std::ofstream file;
  file.open("data/"+filename+".csv");
  file << "pitch,begin,end,duration\n";
  for (int i=0; i < this->detectedNotesArr.size(); i++) {
      Note note = this->detectedNotesArr[i];
      std::cout << "The " << i << " pitch is " << note.pitch.label << " in the "
      << "octave " << note.pitch.octave << " its time begin is " << note.ntp.begin <<
      " its end is " << note.ntp.end << " its duration is " << note.ntp.duration << std::endl;
     file << note.pitch.label << "," << note.ntp.begin << "," << note.ntp.end << "," << note.ntp.duration << "\n";
  }

  file.close();
}
