# Pitch track

Music feature extraction using spectrogram image processing.

## Description

It uses line detection algorithms to estimate the brightest pixel points of spectrogram
imaging melody reconstruction, so we can relate this to time and frequency properties.

### Inputs

Spectrogram images from the `imgs` folder.
(They have been previously filtered, so we remove harmonic content which
 ressonates to logarithmic frequencies)

### Outputs

Estimaed pitches and time properties for the given image in the csv file folder `data`.

## Requirements

- Make
- OpenCV

## Running program

```
make build && make run
```

## Issues

The idea seems promising, but the project still lacks precision to acurately
detect pitches, though the sound between relative notes spaces fits
concisingly according to melody image.

## Improvements

- Translade pitches up and down (+1, -1, command argument)
- Use image name as a program commands argument
- Resolve failed pitch matches.
- Use unit tests
- Set up a logger class with levels for (debug, error, info)

## References

### Installation packages
- [Creating makefile](http://titan.csit.rmit.edu.au/~e20068/teaching/i3dg&a/2016/compiling.html)
- [Installing OpenCV docs](https://docs.opencv.org/4.x/d2/de6/tutorial_py_setup_in_ubuntu.html)

### Building with opencv
 - [Building with opencv](https://stackoverflow.com/a/9097071)
 - [opencv.pc issue](https://stackoverflow.com/a/21524027)

## C++ and libs reference 
 - [Int. opencv with c++](https://www.uio.no/studier/emner/matnat/its/nedlagte-emner/UNIK4690/v18/labs/kompendium_maskinsyn.pdf)
 - [C++ folder structure](https://medium.com/swlh/c-project-structure-for-cmake-67d60135f6f5)
 - [Brighness estimation with opencv](https://stackoverflow.com/questions/14243472/estimate-brightness-of-an-image-opencv)
 - [Pixel buffer data to image recognition opencv](https://stackoverflow.com/questions/4211486/opengl-pixel-data-to-jpeg)

## Knowledge background
- [Image proc. spectrogram 1](https://aircconline.com/sipij/V3N2/3212sipij01.pdf)
- [MIR](https://musicinformationretrieval.com)
