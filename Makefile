CXX=g++
CXXFLAGS=-std=c++11

#opencv flags
CVFLAGS=-I /usr/local/include/opencv4 \
        -L /usr/local/lib \
        `pkg-config --cflags --libs opencv4`


PROGRAM_NAME=main

INC1=include/segment
INC2=include/spectrogram
INC3=include/filter

INC1FLAGS= -I ./$(INC1)
INC2FLAGS= -I ./$(INC2)
INC3FLAGS= -I ./$(INC3)

INCFLAGS=$(INC1FLAGS) $(INC2FLAGS) $(INC3FLAGS)

.RECIPEPREFIX = >

all: clean build run

build: $(PROGRAM_NAME).o spectrogram.o segment.o filter.o
> $(CXX) $(CXXFLAGS) $(PROGRAM_NAME).o filter.o segment.o spectrogram.o -o $(PROGRAM_NAME) $(CVFLAGS) 

$(PROGRAM_NAME).o: src/$(PROGRAM_NAME).cpp $(INC2)/spectrogram.h $(INC1)/segment.h $(INC3)/filter.h
> $(CXX) $(CXXFLAGS) $(CVFLAGS) $(INCFLAGS) -c src/$(PROGRAM_NAME).cpp  

spectrogram.o:
> $(CXX) $(CXXFLAGS) $(CVFLAGS) $(INC2FLAGS) $(INC3FLAGS) -c $(INC2)/spectrogram.cpp

segment.o: $(INC3)
> $(CXX) $(CXXFLAGS) $(CVFLAGS) $(INC1FLAGS) $(INC3FLAGS) -c $(INC1)/segment.cpp

filter.o:
> $(CXX) $(CXXFLAGS) $(CVFLAGS) $(INC3FLAGS) -c $(INC3)/filter.cpp

run:
> ./$(PROGRAM_NAME)

clean:
> rm *.o $(PROGRAM_NAME)
