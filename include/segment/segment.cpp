#include "segment.h"
#include "filter.h"
#include <iostream>

Segment::Segment(std::string filename) {
  /*
     Segmentation constructor
 */

  image = cv::imread(filename);
  imageheight = image.cols;
  imagewidth = image.rows;

  /*Initialize the image segmentation
    for line detection
   */

  std::vector<cv::Vec4f> temp_lines;
  cv::Mat lines;
  cv::Ptr<cv::LineSegmentDetector> det;
  cv::cvtColor(image, lines, cv::COLOR_BGR2GRAY);
  det = cv::createLineSegmentDetector();
  det->detect(lines, temp_lines);

  this->linesegments = temp_lines;
  this->det = det;
}

int Segment::getImageHeight() {
  return this->imageheight;
}

void Segment::filterSegments() {
  this->linesegments = filteredSegments(this->linesegments);
}

int Segment::getImageWidth() {
  return this->imagewidth;
} 

std::vector<std::array<float, 4>> Segment::getLineSegments() {
   this->filterSegments();

   std::vector<std::array<float, 4>> segments;

   for (int i=0; i < this->linesegments.size(); i++) {
      std::array<float, 4> coordinates{
            {
              this->linesegments[i][0],
              this->linesegments[i][1],
              this->linesegments[i][2],
              this->linesegments[i][3],
            }
      };
      segments.push_back(coordinates);
   }

   return segments;
}

void Segment::showImage() {
  this->det->drawSegments(this->image, this->linesegments);
  cv::imshow("img", this->image);
  cv::waitKey(0);
}
