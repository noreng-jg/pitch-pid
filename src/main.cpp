#include <iostream>
#include <vector>
#include <array>
#include "spectrogram.h"
#include "segment.h"

int main() {
  std::string file = "misty";
  Segment seg("imgs/"+file+".png");
  Spectrogram spec(seg.getImageHeight(), seg.getImageWidth(), 8);
  std::vector<std::array<float, 4>> detectedSegments;
  detectedSegments = seg.getLineSegments();
  spec.noteDetector(detectedSegments);
  spec.sortNotes();
  spec.saveDetectedNotes(file);
  seg.showImage();
  return 0;
}
