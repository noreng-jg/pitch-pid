#ifndef SPECTROGRAM_H
#define SPECTROGRAM_H
#include <vector>
#include <fstream>
#include <math.h>
#include <map>
#include <iterator>
#include <string>

typedef struct {
  int octave;
  std::string label; // fullname with octave
} Pitch;

typedef struct {
  float begin;
  float end;
  float duration;
} NoteTimeProperties;

typedef struct {
  Pitch pitch;
  NoteTimeProperties ntp;
} Note;

typedef struct {
  int highestpixel;  //highest freq pixel
  int lowestpixel;  //lowest freq pixel
  double fmax;
  double fmin;
  int num; // between 0 and 6
  float freqratio ; // how much freq variation per pixel
  int limpowermin;
  int limpowermax; // max log power from spec in range -> max 12 -> 4096
} OctaveRange;

class Spectrogram {
  public:
    Spectrogram(int h, int w, float timed);
    int getHeight();
    void printOctaveRangesInfo();
    float estimatedFreq(float pi);
    Pitch pitchDetector(float frequency);
    void noteDetector(std::vector<std::array<float, 4>> pointCoordinates);
    NoteTimeProperties evaluateNoteTimeProperties(float begin, float end);
    void saveDetectedNotes(std::string);
    void sortNotes();
  private:
    int height;
    int width;
    float timeduration;
    std::vector<Note> detectedNotesArr;
    std::vector<OctaveRange> octaveRanges;
    std::map<float, Pitch> mapFrequencies = {
      {41.20, Pitch{1,"E1"}},
      {43.65, Pitch{1,"F1"}},
      {46.25, Pitch{1,"F#1/Gb1"}},
      {49.00, Pitch{1,"G1"}},
      {51.91, Pitch{1,"G#1/Ab1"}},
      {55.00, Pitch{1,"A1"}},
      {58.27, Pitch{1, "A#1/Bb1"}},
      {61.74, Pitch{1,"B1"}},
      {65.41, Pitch{2,"C2"}},
      {69.30, Pitch{2,"C#2/Db2"}},
      {73.42, Pitch{2,"D2"}},
      {77.78, Pitch{2,"D#2/Eb2"}},
      {82.41, Pitch{2,"E2"}},
      {87.31, Pitch{2,"F2"}},
      {92.50, Pitch{2,"F#2/Gb2"}},
      {98.00, Pitch{2,"G2"}},
      {103.83, Pitch{2,"G#2/Ab2"}},
      {110.00, Pitch{2,"A2"}},
      {116.54, Pitch{2,"A#2/Bb2"}},
      {123.47, Pitch{2,"B2"}},
      {130.81, Pitch{3,"C3"}},
      {138.59, Pitch{3,"C#3/Db3"}},
      {146.83, Pitch{3,"D3"}},
      {155.56, Pitch{3,"D#3/Eb3"}},
      {164.81, Pitch{3,"E3"}},
      {174.61, Pitch{3,"F3"}},
      {185.00, Pitch{3,"F#3/Gb3"}},
      {196.00, Pitch{3,"G3"}},
      {207.65, Pitch{3,"G#3/Ab3"}},
      {220.00, Pitch{3,"A3"}},
      {233.08, Pitch{3,"A#3/Bb3"}},
      {246.94, Pitch{3,"B3"}},
      {261.63, Pitch{4,"C4"}},
      {277.18, Pitch{4,"C#4/Db4"}},
      {293.66, Pitch{4,"D4"}},
      {311.13, Pitch{4,"D#4/Eb4"}},
      {329.63, Pitch{4,"E4"}},
      {349.23, Pitch{4,"F4"}},
      {369.99, Pitch{4,"F#4/Gb4"}},
      {392.00, Pitch{4,"G4"}},
      {415.30, Pitch{4,"G#4/Ab4"}},
      {440.00, Pitch{4,"A4"}},
      {466.16, Pitch{4,"A#4/Bb4"}},
      {493.88, Pitch{4,"B4"}},
      {523.25, Pitch{5,"C5"}},
      {554.37, Pitch{5,"C#5/Db5"}},
      {587.33, Pitch{5,"D5"}},
      {622.25, Pitch{5,"D#5/Eb5"}},
      {659.25, Pitch{5,"E5"}},
      {698.46, Pitch{5,"F5"}},
      {739.99, Pitch{5,"F#5/Gb5"}},
      {783.99, Pitch{5,"G5"}},
      {830.61, Pitch{5,"G#5/Ab5"}},
      {880.00, Pitch{5,"A5"}},
      {932.33, Pitch{5,"A#5/Bb5"}},
      {987.77, Pitch{5,"B5"}},
      {1046.50, Pitch{6,"C6"}},
      {1108.73, Pitch{6,"C#6/Db6"}},
      {1174.66, Pitch{6,"D6"}},
      {1244.51, Pitch{6,"D#6/Eb6"}},
      {1318.51, Pitch{6,"E6"}},
      {1396.91, Pitch{6,"F6"}},
      {1479.98, Pitch{6,"F#6/Gb6"}},
      {1567.98, Pitch{6,"G6"}},
      {1661.22, Pitch{6,"G#6/Ab6"}},
      {1760.00, Pitch{6,"A6"}},
      {1864.66, Pitch{6,"A#6/Bb6"}},
      {1975.53, Pitch{6,"B6"}},
      {2093.00, Pitch{7,"C7"}},
      {2217.46, Pitch{7,"C#7/Db7"}},
      {2349.32, Pitch{7,"D7"}},
      {2489.02, Pitch{7,"D#7/Eb7"}},
      {2637.02, Pitch{7,"E7"}},
      {2793.83, Pitch{7,"F7"}},
      {2959.96, Pitch{7,"F#7/Gb7"}},
      {3135.96, Pitch{7,"G7"}},
      {3322.44, Pitch{7,"G#7/Ab7"}},
      {3520.00, Pitch{7,"A7"}},
      {3729.31, Pitch{7,"A#7/Bb7"}},
      {3951.07, Pitch{7,"B7"}},
      {4186.01, Pitch{8,"C8"}}
    };
};

#endif
