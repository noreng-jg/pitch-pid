#include <iostream>
#include "filter.h"

bool belowPercentageLimit(float v1, float v2, float perc) {
  return (v1 - v2)/100 < perc;
}

float _module(float val){
  return (val < 0) ? (-val) : val;
}

float evaluateCloserFrequency(float liminf, float limsup, float number) {
  std::cout << "liminf: " << liminf << "limsup: " <<limsup << std::endl;
  return (limsup - number) <= (number - liminf) ? limsup : liminf;
}

bool isBetween(float liminf, float limsup, float number) {
  return number >= liminf && number <= limsup;
}

bool inAPreviousRange(Points *p, cv::Vec4f &point) {
  /*
     This function filter removes similar notes that are in a retangular
     base and update the limits to fit the time interval to the maximum size
  */

  float previousMin = (p->v1x < p->v2x) ? p->v1x : p->v2x;
  float previousMax = (p->v1x > p->v2x) ? p->v1x : p->v2x;
  float pointMin = (p->x1 < p->x2) ? p->x1 : p->x2;
  float pointMax = (p->x1 > p->x2) ? p->x1 : p->x2;

  std::cout << previousMin << " " << previousMax << " " << pointMin << " " << pointMax << std::endl;

  if (pointMax >= previousMax && pointMin <= previousMin) {
    point[0] =  pointMin;
    point[2] =  pointMax;
    point[1] =  (p->v1y + p->y1)/2;
    return true;
  } else if (pointMax > previousMin && previousMax > pointMax) {
    point[0] =  pointMin;
    point[1] =  (p->v1y + p->y1)/2;
    return true;
  } else if (pointMin > previousMin && pointMin  < previousMax) {
    point[2] =  pointMax;
    point[3] =  (p->y2 + p->v2y)/2;
    // std::cout << previousMin << " " << previousMax << " " << pointMin << " " << pointMax << std::endl;
    std::cout << "\nEntered here 2" <<std::endl;
    return true;
  }

  return false;
}

bool repeatedNote(Points *p) {
  /*
     This function ensures that no duplications occur in time
     domain
  */

  float percentage = 0.01;

  float diffx1 = _module((p->x1 - p->v1x)/100);
  float diffx2 = _module((p->x1 - p->v2x)/100);
  float diffx3 = _module((p->x2 - p->v1x)/100);
  float diffx4 = _module((p->x2 - p->v2x)/100);

  bool check1 = (diffx1 < percentage || diffx2 < percentage);
  bool check2 = (diffx3 < percentage || diffx4 < percentage);

  return check1 && check2;
}

bool hasVerticalLines(int y1, int y2) {
  /*
     Detects vertical discrepancies
     in spectrogram
   */

  float mod = y1 - y2;

  if (mod < 0) {
    float newmod = - mod;
    mod = newmod;
  }

  return (mod)/100 > 0.0001;
}

void insertionControl(cv::Vec4f point, std::vector<cv::Vec4f> &vec) {
  for (int i=0; i < vec.size(); i++) {
    cv::Vec4f v = vec[i];
    cv::Point from(v[0], v[1]);
    cv::Point to(v[2], v[3]);

    Points p{
      x1: float(point[0]),
      y1: float(point[1]),
      x2: float(point[2]),
      y2: float(point[3]),
      v1x: v[0],
      v1y: v[1],
      v2x: v[2],
      v2y: v[3],
      };

    if (inAPreviousRange(&p, vec[i])) {
      std::cout << vec[i] << std::endl;
      return;
    }

    if (repeatedNote(&p)) {
      vec[i][1] = (p.y1 + p.v1y)/2;
      vec[i][3] = (p.y2 + p.v2y)/2;

      return;
    }
  }
  vec.push_back(point);
}

std::vector<cv::Vec4f> filteredSegments(std::vector<cv::Vec4f> lines) {
   std::vector<cv::Vec4f> filtered_lines;

   for (int i=0; i<lines.size(); i++) {
        cv::Vec4f v = lines[i];
        cv::Point from(v[0], v[1]);
        cv::Point to(v[2], v[3]);

        if (!hasVerticalLines(v[1], v[3])) {
          insertionControl(v, filtered_lines);
        }
    }

   return filtered_lines;
}
